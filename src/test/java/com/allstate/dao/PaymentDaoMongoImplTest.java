package com.allstate.dao;

import com.allstate.entity.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.util.Assert;

import java.util.Date;


@SpringBootTest
class PaymentDaoMongoImplTest {

    @Autowired
    IPaymentDao dao;

    @Autowired
    MongoTemplate tpl;

    @BeforeEach
    void setUp() {
        tpl.dropCollection(Payment.class);
        dao.save(new Payment(1,new Date(),"CHEQUE",10, 7));
    }

    @Test
    void rowCount() {
        Assert.isTrue(1==dao.rowCount(),"! Payment returned");
    }

    @Test
    void findById() {
        Assert.isTrue(1==dao.findById(1).getId(),"Found correct payment id = 1");
    }

    @Test
    void findByType() {
        dao.save(new Payment(2,new Date(),"CHEQUE",1559986774.01, 7));
        Assert.isTrue(2==dao.findByType("CHEQUE").size(),"Found two CHEQUE payments");
    }

    @Test
    void save() {
        Assert.isTrue(1==dao.save(new Payment(2,new Date(),"CHEQUE",1559986774.01, 7))
        , "Successfully saved Payment");
    }

    @Test
    void findAll_Success() {
        Assert.isTrue(1==dao.findAll().size(),"We have at exactly one payment in Mongo");
    }

}