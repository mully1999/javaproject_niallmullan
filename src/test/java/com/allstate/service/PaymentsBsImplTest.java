package com.allstate.service;

import com.allstate.entity.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
class PaymentsBsImplTest {

    @Autowired
    MongoTemplate tpl;

    @Autowired
    IPaymentBs service;


    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date date = new Date();

    @BeforeEach
    void setUp() {
        tpl.dropCollection(Payment.class);
        service.save(new Payment(1,date,"CHEQUE",10, 7));
    }

    @Test
    void rowCount() {
        Assert.isTrue(1==service.rowcount(),"! Payment returned");
    }

    @Test
    void findById_goodId() {
        Assert.isTrue(1==service.findById(1).getId(),"Found correct payment id = 1");
    }

    @Test
    void findById_badId() {
        Assert.isTrue(null==service.findById(-20),"Invalid payment id handled");
    }

    @Test
    void findByType_goodType() {
        service.save(new Payment(2,date,"CHEQUE",1559986774.01, 7));
        Assert.isTrue(2==service.findByType("CHEQUE").size(),"Found two CHEQUE payments");
    }

    @Test
    void findAll_Success() {
        service.save(new Payment(2,date,"CHEQUE",1559986774.01, 7));
        Assert.isTrue(2==service.findAllPayments().size(),"Found two payments");
    }

    @Test
    void findByType_nullType() {
        Assert.isTrue(null == service.findByType(null),"null TYPE handled");
    }

    @Test
    void findByType_emptyType() {
        Assert.isTrue(null == service.findByType(""),"Empty TYPE handled");
    }

    @Test
    void save_good() {
        Assert.isTrue(1==service.save(new Payment(2,date,"CHEQUE",1559986774.01, 7))
                , "Successfully saved Payment");
    }

    @Test
    void save_null() {
        Assert.isTrue(0==service.save(null)
                , "null Payment handled");
    }

}