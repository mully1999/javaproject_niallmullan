package com.allstate.dao;

import com.allstate.entity.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentDaoMongoImpl implements IPaymentDao{

    @Autowired
    private MongoTemplate tpl;

    @Override
    public int rowCount() {
        Query query = new Query();
        Long result = tpl.count(query, Payment.class);
        return result.intValue();
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = tpl.findOne(query, Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> payments = tpl.find(query, Payment.class);
        return payments;
    }

    @Override
    public List<Payment> findAll() {
        List<Payment> payments = tpl.findAll(Payment.class);
        return payments;
    }

    @Override
    /**
     * Attempts to save an object in the Mongo Collection.
     * @Param Payment object
     * @return int 1 indicates a success 0 indicates a fail.
     */
    public int save(Payment payment) {
        int result = 1;
        try{
            tpl.save(payment);
        } catch (Exception ex){
            result = 0;
        }finally{
            return result;
        }
    }
}
