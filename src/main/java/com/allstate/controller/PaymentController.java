package com.allstate.controller;

import com.allstate.App;
import com.allstate.entity.Payment;
import com.allstate.service.IPaymentBs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
@RequestMapping(value = "/payme")
public class PaymentController {

    @Autowired
    IPaymentBs service;

    //Disabled so I could mock for testing
    //private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    private final Logger LOGGER = LoggerFactory.getLogger(App.class);

    @RequestMapping(value = "/find/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> getPaymentById(@PathVariable("id")int id ){
        LOGGER.info("Entered find by id method of controller");
        Payment payment = service.findById(id);
        if (payment ==null) {
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public ResponseEntity<String> getStatus() {
        LOGGER.info("Entered status method of controller");
        return new ResponseEntity<String>("Payment Service Up & Running", HttpStatus.OK);
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public ResponseEntity<Integer> getRowCount() {

        LOGGER.info("Entered count method of controller");
        Integer result;
        try {
            return new ResponseEntity<Integer>(service.rowcount(), HttpStatus.OK);
        } catch (Exception ex) {
            LOGGER.error("Exception occurred in count method of controller");
            return new ResponseEntity<Integer>(0, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity<String> findById(@RequestBody Payment payment){
        LOGGER.info("Entered save method of controller");
        int result;
        try {
            if (1==service.save(payment))
                return new ResponseEntity<String>("Your Payment was successfully saved", HttpStatus.OK);
            else
                return new ResponseEntity<String>("There was an issue saving your payment, please retry", HttpStatus.INTERNAL_SERVER_ERROR);
        }catch (Exception ex)
        {
            LOGGER.error("Exception occurred in save method of controller");
            return new ResponseEntity<String>("There was an issue saving your payment, please retry", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @RequestMapping(value = "/find/type/{type}", method = RequestMethod.GET)
    public ResponseEntity<List<Payment>> getPaymentByType(@PathVariable("type")String type){
        LOGGER.info("Entered find by type method of controller");
        List<Payment> payments = service.findByType(type);
        if (payments == null || payments.size() < 1) {
            return new ResponseEntity<List<Payment>>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List<Payment>>(payments, HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public ResponseEntity<List<Payment>> getAllPayments(){
        LOGGER.info("Entered find all method of controller");

        List<Payment> payments = service.findAllPayments();
        if (payments == null || payments.size() < 1) {
            return new ResponseEntity<List<Payment>>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List<Payment>>(payments, HttpStatus.OK);
        }
    }

    //Add some controller level exception handling
    @ExceptionHandler(Exception.class)
    protected ResponseEntity<String> handleException(Exception ex, HttpServletRequest request) {
        return new ResponseEntity<String>("An Exception occured with message: " +
                ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
