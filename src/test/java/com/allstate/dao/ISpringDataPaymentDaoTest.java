package com.allstate.dao;

import com.allstate.entity.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;
import java.util.Date;

@SpringBootTest
class ISpringDataPaymentDaoTest {

    @Autowired
    ISpringDataPaymentDao repo;

    @BeforeEach
    void setUp() {
        repo.deleteAll();
        repo.insert(new Payment(1,new Date(),"CHEQUE",10, 7));
    }

    @Test
    void rowCount() {
        repo.insert(new Payment(2,new Date(),"CASH",10, 7));
        Assert.isTrue(2==repo.count(), "One row returned");
    }

    @Test
    void findById() {
        Assert.isTrue( repo.findById(1) instanceof Payment,"We found a Payment object");
        Assert.isTrue( 1==repo.findById(1).getId(),"Payment object has ID 1");
    }

    @Test
    void findByType() {
        repo.insert(new Payment(2,new Date(),"CHEQUE",10, 7));
        Assert.isTrue(2==repo.findByType("CHEQUE").size(), "Two Payment objects returned with CHEQUE Type");
    }

    @Test
    void save() {
        repo.save(new Payment(2,new Date(),"CHEQUE",10, 7));
        Assert.isTrue(2==repo.count(), "Our save worked");
    }
}