package com.allstate.controller;

import com.allstate.service.IPaymentBs;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.ReflectionUtils;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import org.slf4j.Logger;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

//@ExtendWith(SpringExtension.class)
//@WebMvcTest(controllers = PaymentController.class)
class PaymentControllerTest {

    private MockMvc mvc;
    private PaymentController paymentController;

    @Mock
    private Logger mockLogger;

    @BeforeEach
    void setUp() {
        paymentController = new PaymentController();
        initMocks(this);
        mvc = standaloneSetup(paymentController).build();
        ReflectionTestUtils.setField(paymentController,"LOGGER",mockLogger);
    }

    @Test
    void getStatus() throws Exception {
        mvc.perform(get("/payme/status")
                .contentType("application/json"))
                .andExpect(status().isOk());

        verify(mockLogger).info("Entered status method of controller");
    }


}