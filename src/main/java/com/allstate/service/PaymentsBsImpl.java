package com.allstate.service;

import com.allstate.dao.IPaymentDao;
import com.allstate.entity.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class PaymentsBsImpl implements IPaymentBs{

    @Autowired
    IPaymentDao dao;

    @Override
    public int rowcount() {
        return dao.rowCount();
    }

    @Override
    public Payment findById(int id) {
        if (id > -1)
            return dao.findById(id);
        else
            return null;
    }

    @Override
    public List<Payment> findByType(String type) {
        if (!StringUtils.isEmpty(type))
            return dao.findByType(type);
        else
            return null;
    }

    @Override
    public List<Payment> findAllPayments() {
        return dao.findAll();
    }

    @Override
    public int save(Payment payment) {
        if (payment != null)
            return dao.save(payment);
        else
            return 0;
    }
}
