package com.allstate.dao;

import com.allstate.entity.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

/** This class was written to see how the SpringData implementation for Mongo worked
 * it was not used by the other layers, but a test class was written for it.
 */

@Repository
public interface ISpringDataPaymentDao extends MongoRepository<Payment, Integer> {

    //int rowCount();                       //Just use count from SpringData
    Payment findById(int id);             //Already exists in SpringData
    List<Payment> findByType(String type);  //Let spring data build the query
    //Integer save(Payment payment);            //Already exists in SpringData
}
