package com.allstate.dao;

import com.allstate.entity.Payment;

import java.util.List;

public interface IPaymentDao {

    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    List<Payment> findAll();
    int save(Payment payment);

}
