package com.allstate.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Payment {

    @Id
    private int id;
    private Date paymentDate;
    private String type;
    private double amount;
    private int custId;

    @Override
    public String toString(){
        return String.format("ID: %d DATE: %s TYPE: %s, AMOUNT: %f CUSTOMER ID: %d",
                    this.getId(), this.getPaymentDate(), this.getType(), this.getAmount(), this.getCustId());
    }


}
