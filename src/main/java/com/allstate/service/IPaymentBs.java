package com.allstate.service;

import com.allstate.entity.Payment;

import java.util.List;

public interface IPaymentBs {
    int rowcount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    List<Payment> findAllPayments();
    int save(Payment payment);

}
