package com.allstate.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class PaymentTest {

    Payment payment;
    String myToString;

    @BeforeEach
    void setUp() {
        payment = new Payment(1,new Date(),"CASH",50.55d,1);
        myToString = payment.toString();
    }

    @Test
    void testPaymentPopulatedCorrectly() {
        Assert.notNull(payment, "Payment Object Populated");
        Assert.isTrue(1 == payment.getId(),"Payment ID Match");
        Assert.isTrue("CASH".equals(payment.getType()),"Payment TYPE Match");
        Assert.isTrue(50.55 == payment.getAmount(),"Payment Amount Match");
        Assert.isTrue(1 == payment.getCustId(),"CUSTOMER ID Match");
        //DateTime changes so not testing this toString
        //Assert.isTrue("ID: 1 DATE: Mon Sep 14 11:11:04 UTC 2020 TYPE: CASH, AMOUNT: 50.550000 CUSTOMER ID: 1".equals(myToString), "TO STRING WORKING");


                //return String.format("ID: %d DATE: %s TYPE: %s, AMOUNT: %d CUSTOMER ID: %d",
        //                    this.getId(), this.getPaymentDate(), this.getType(), this.getAmount(), this.getCustId());
    }


}